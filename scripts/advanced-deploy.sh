#!/usr/bin/env bash

# === User settings: Update these ===
DB_NAME=mayan_edms
DB_USERNAME=mayan
DB_PASSWORD=test123
# ===== End of user settings ========

# === Don't change these unless you know what you are doing ======
INSTALLATION_DIRECTORY=/usr/share/mayan-edms/
LOG_DIRECTORY=/var/log/mayan/

NGINX_CLIENT_MAX_BODY_SIZE=500M
NGINX_PROXY_READ_TIMEOUT=600s

# === End of configuration ====

if [ -d ${INSTALLATION_DIRECTORY} ]; then
    echo "Mayan EDMS already installed, exiting.\n"
    exit 0
fi

echo -e "\n -> Configure the OS locale \n"
echo "LC_ALL=\"en_US.UTF-8\"" | sudo tee --append /etc/default/locale
sudo locale-gen en_US.UTF-8
sudo update-locale LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

echo -e "\n -> Running apt-get update & upgrade \n"
sudo apt-get -qq update
sudo apt-get -y upgrade

echo -e "\n -> Installing core binaries \n"
sudo apt-get install -y graphviz nginx supervisor redis-server postgresql \
libpq-dev libjpeg-dev libmagic1 libpng-dev libreoffice \
libtiff-dev gcc ghostscript gnupg python-dev python-virtualenv \
tesseract-ocr poppler-utils -y

echo -e "\n -> Setting up virtualenv \n"
sudo rm -f ${INSTALLATION_DIRECTORY} -R
sudo virtualenv ${INSTALLATION_DIRECTORY}
source ${INSTALLATION_DIRECTORY}bin/activate

echo -e "\n -> Making the installation directory readable and writable for the current user \n"
sudo chown `whoami` ${INSTALLATION_DIRECTORY} -R

echo -e "\n -> Installing Mayan EDMS from PyPI \n"
pip install mayan-edms

echo -e "\n -> Installing Python client for PostgreSQL, Redis, and uWSGI \n"
pip install psycopg2 redis uwsgi

echo -e "\n -> Creating the database for the installation \n"
echo "CREATE USER mayan WITH PASSWORD '$DB_PASSWORD';" | sudo -u postgres psql
sudo -u postgres createdb -O $DB_USERNAME $DB_NAME

echo -e "\n Add the database creation permission to the mayan to be able to create test database \n"
sudo -u postgres sh -c 'echo "ALTER USER $DB_USERNAME CREATEDB;" | psql '

echo -e "\n -> Creating the directories for the logs \n"
sudo mkdir ${LOG_DIRECTORY}

echo -e "\n -> Grant permissions to the webserver user for the log directory \n"
sudo chgrp www-data ${LOG_DIRECTORY} -R
sudo chmod g+w ${LOG_DIRECTORY} -R

echo -e "\n -> Making a convenience symlink \n"
cd ${INSTALLATION_DIRECTORY}
ln -s lib/python2.7/site-packages/mayan .

echo -e "\n -> Creating an initial settings file \n"
mayan-edms.py createsettings

echo -e "\n -> Updating the mayan/settings/local.py file \n"
cat >> mayan/settings/local.py << EOF
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '$DB_NAME',
        'USER': '$DB_USERNAME',
        'PASSWORD': '$DB_PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

BROKER_URL = 'redis://127.0.0.1:6379/0'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/0'
EOF

echo -e "\n -> Migrating the database or initialize the project \n"
mayan-edms.py initialsetup

echo -e "\n -> Disabling the default NGINX site \n"
sudo rm -f /etc/nginx/sites-enabled/default

echo -e "\n -> Creating a uwsgi.ini file \n"
cat > uwsgi.ini << EOF
[uwsgi]
chdir = ${INSTALLATION_DIRECTORY}lib/python2.7/site-packages/mayan
chmod-socket = 664
chown-socket = www-data:www-data
env = DJANGO_SETTINGS_MODULE=mayan.settings.production
gid = www-data
logto = ${LOG_DIRECTORY}uwsgi-%n.log
pythonpath = ${INSTALLATION_DIRECTORY}lib/python2.7/site-packages
master = True
max-requests = 5000
socket = ${INSTALLATION_DIRECTORY}uwsgi.sock
uid = www-data
vacuum = True
wsgi-file = ${INSTALLATION_DIRECTORY}lib/python2.7/site-packages/mayan/wsgi.py
EOF

echo -e "\n -> Creating the NGINX site file for Mayan EDMS, /etc/nginx/sites-available/mayan \n"
sudo sh -c "cat > /etc/nginx/sites-available/mayan << EOF
server {
    listen 80;
    server_name localhost;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:${INSTALLATION_DIRECTORY}uwsgi.sock;

        client_max_body_size ${NGINX_CLIENT_MAX_BODY_SIZE};
        proxy_read_timeout ${NGINX_PROXY_READ_TIMEOUT};
    }

    location /static {
        alias ${INSTALLATION_DIRECTORY}mayan/media/static;
        expires 1h;
    }

    location /favicon.ico {
        alias ${INSTALLATION_DIRECTORY}mayan/media/static/appearance/images/favicon.ico;
        expires 1h;
    }
}
EOF"

echo -e "\n -> Enabling the NGINX site for Mayan EDMS \n"
sudo ln -s /etc/nginx/sites-available/mayan /etc/nginx/sites-enabled/

echo -e "\n -> Creating the supervisor file for the uWSGI process, /etc/supervisor/conf.d/mayan-uwsgi.conf \n"
sudo sh -c "cat > /etc/supervisor/conf.d/mayan-uwsgi.conf << EOF
[program:mayan-uwsgi]
autorestart = true
autostart = true
command = ${INSTALLATION_DIRECTORY}bin/uwsgi --ini ${INSTALLATION_DIRECTORY}uwsgi.ini
redirect_stderr = true
user = www-data
EOF"

echo -e "\n -> Creating the supervisor file for the Celery worker, /etc/supervisor/conf.d/mayan-celery.conf \n"
sudo sh -c "cat > /etc/supervisor/conf.d/mayan-celery.conf << EOF
[program:mayan-worker-fast]
autorestart = true
autostart = true
command = ${INSTALLATION_DIRECTORY}bin/python ${INSTALLATION_DIRECTORY}bin/mayan-edms.py celery --settings=mayan.settings.production worker -Ofair -l ERROR -Q converter -n mayan-worker-fast.%%h
directory = ${INSTALLATION_DIRECTORY}
killasgroup = true
priority = 998
startsecs = 10
stderr_logfile = ${LOG_DIRECTORY}worker-fast-stderr.log
stdout_logfile = ${LOG_DIRECTORY}worker-fast-stdout.log
stopwaitsecs = 10
user = www-data

[program:mayan-worker-medium]
autorestart = true
autostart = true
command = ${INSTALLATION_DIRECTORY}bin/python ${INSTALLATION_DIRECTORY}bin/mayan-edms.py celery --settings=mayan.settings.production worker -Ofair -l ERROR -Q checkouts_periodic,documents_periodic,indexing,metadata,sources,sources_periodic,uploads,documents -n mayan-worker-medium.%%h
directory = ${INSTALLATION_DIRECTORY}
killasgroup = true
priority = 998
startsecs = 10
stderr_logfile = ${LOG_DIRECTORY}worker-medium-stderr.log
stdout_logfile = ${LOG_DIRECTORY}worker-medium-stdout.log
stopwaitsecs = 10
user = www-data

[program:mayan-worker-slow]
autorestart = true
autostart = true
command = ${INSTALLATION_DIRECTORY}bin/python ${INSTALLATION_DIRECTORY}bin/mayan-edms.py celery --settings=mayan.settings.production worker -Ofair -l ERROR -Q mailing,parsing,ocr,tools,statistics -n mayan-worker-slow.%%h --concurrency=1
directory = ${INSTALLATION_DIRECTORY}
killasgroup = true
numprocs = 1
priority = 998
startsecs = 10
stderr_logfile = ${LOG_DIRECTORY}worker-slow-stderr.log
stdout_logfile = ${LOG_DIRECTORY}worker-slow-stdout.log
stopwaitsecs = 10
user = www-data

[program:mayan-beat]
autorestart = true
autostart = true
command = ${INSTALLATION_DIRECTORY}bin/python ${INSTALLATION_DIRECTORY}bin/mayan-edms.py celery --settings=mayan.settings.production beat -l ERROR
directory = ${INSTALLATION_DIRECTORY}
numprocs = 1
killasgroup = true
priority = 998
startsecs = 10
stderr_logfile = ${LOG_DIRECTORY}beat-stderr.log
stdout_logfile = ${LOG_DIRECTORY}beat-stdout.log
stopwaitsecs = 1
user = www-data
EOF"

echo -e "\n -> Collecting the static files \n"
mayan-edms.py collectstatic --noinput

echo -e "\n -> Making the installation directory readable and writable by the webserver user \n"
sudo chown www-data:www-data ${INSTALLATION_DIRECTORY} -R

echo -e "\n -> Remove /tmp/mayan_locks.tmp created during installation \n"
sudo rm -f /tmp/mayan_locks.tmp

echo -e "\n -> Restarting the services \n"
sudo systemctl enable supervisor
sudo systemctl restart supervisor
sudo systemctl restart nginx
