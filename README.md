# Mayan EDMS Vagrant box

This repository provides Vagrant box setup for an advanced deployment.

## Install Vagrant
    
    $ sudo apt-get -y install vagrant

## Start and provision a vagrant box

    $ vagrant up advanced-deploy

browse to http://localhost:8080

For convinience a Makefile is also provided that can start, destroy and run tests inside the image.
